package dkit.gd2.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainApp {
    public static void main(String[] args) {
        String string = "I am a string. So I am";
        System.out.println(string);
        String yourString = string.replaceAll("I", "you");
        String areString = yourString.replaceAll("am", "are");
        System.out.println(areString);

        String randomString = "abcDeeeeabcDeee1456iiii8888s";
        System.out.println(randomString.replaceAll(".", "Y"));

        System.out.println(randomString.replaceAll("^abcDeee", "YYY"));
        System.out.println(randomString.matches("^Hello"));
        System.out.println(randomString.matches("abc.*"));

        System.out.println(randomString.replaceAll("i8888s$", "The end"));
        System.out.println(randomString.replaceAll("[aeiAEI]", "X"));
        System.out.println(randomString);
//        String replaceAB = randomString.replaceAll("[a][b]", "Xb");
//        String replaceABi8 = replaceAB.replaceAll("[i][8]", "X8");
//        System.out.println(replaceABi8);
        System.out.println(randomString.replaceAll("[ai][b8]", "XX"));
        String filepath = "<Book/Aircraft.hpp>";
        System.out.println(filepath.replaceAll("<Book/(.+)>", "\"$1\""));

        System.out.println("harry".replaceAll("[Hh]arry", "Harry"));

        String newString = "abcDeeeeF12Ghiiiiiiiijk199z";
        System.out.println(newString.replaceAll("[^ej]", "X"));
        System.out.println(newString.replaceAll("[a-f3-8]", "X"));
        System.out.println(newString.replaceAll("[a-fA-F3-8]", "X"));
        System.out.println(newString.replaceAll("(?i)[a-f3-8]", "X"));

        System.out.println(newString.replaceAll("[0-9]", "X"));
        System.out.println(newString.replaceAll("\\d", "X"));
        System.out.println(newString.replaceAll("\\D", "X"));

        String hasWhiteSpace = "I have blanks and \t a tab, and a newline\n";
        System.out.println(hasWhiteSpace);
        System.out.println(hasWhiteSpace.replaceAll("\\s", ""));
        System.out.println(hasWhiteSpace.replaceAll("\\t ", ""));
        System.out.println(hasWhiteSpace.replaceAll("\\S", "X"));

        newString = "abcDeeeeF12Ghiiiiiiiijk199z*@&";
        System.out.println(newString.replaceAll("\\w", "X"));
        System.out.println(hasWhiteSpace.replaceAll("\\w", "X"));
        System.out.println(hasWhiteSpace.replaceAll("\\S", "X"));

        System.out.println(hasWhiteSpace.replaceAll("\\b", "X"));
        System.out.println(newString.replaceAll("\\W", "X"));

        System.out.println(newString.replaceAll("^abcDe{3}", "YYY"));
        System.out.println(newString.replaceAll("^abcDe+", "YYY"));
        System.out.println(newString.replaceAll("^abcDe*", "YYY"));
        System.out.println(newString.replaceAll("^abcDe{2,5}", "YYY"));

        //newString = "abcDeeeeF12Ghiiiiiiiijk199z*@&";
        System.out.println(newString.replaceAll("hi*j", "YYY"));

        StringBuilder htmlTest = new StringBuilder("<h1>My Heading</h1>");
        htmlTest.append("<h2>Sub-heading</h2>");
        htmlTest.append("<p>This is a paragraph</p>");
        htmlTest.append("<p>This is another paragraph</p>");
        htmlTest.append("<h2>Summary</h2>");
        htmlTest.append("<p>This is the summary</p>");
        System.out.println(htmlTest);

        String h2Pattern = "<h2>";
        Pattern pattern = Pattern.compile(h2Pattern);
        Matcher matcher = pattern.matcher(htmlTest);
        System.out.println(matcher.matches());

        matcher.reset();

        int count = 0;
        while(matcher.find()){
            count++;
            System.out.println("Occurrence " + count + " : " + matcher.start() + " to " + matcher.end());
        }

        String h2GroupPattern = "(<h2>.*?</h2>)";
        Pattern groupPattern = Pattern.compile(h2GroupPattern);
        Matcher groupMatcher = groupPattern.matcher(htmlTest);
        System.out.println(groupMatcher.matches());

        groupMatcher.reset();

        while(groupMatcher.find()){
            System.out.println("Occurrence: " + groupMatcher.group(1));
        }

        String h2TextGroup = "(<h2>)(.*?)(</h2>)";
        Pattern textPattern = Pattern.compile(h2TextGroup);
        Matcher textMatcher = textPattern.matcher(htmlTest);
        System.out.println(textMatcher.matches());

        textMatcher.reset();

        while(textMatcher.find()){
            System.out.println("Occurrence: " + textMatcher.group(2));
        }

        //Logical operators
        //abc means a and b and c

        //Not operator = [^]
        String tvTest = "tstvkt";
        //Find all t's that are not immediately followed by a v

        String tNotVRegExp = "t(?!v)";

        Pattern tNotVPattern = Pattern.compile(tNotVRegExp);
        Matcher tNotVMatcher = tNotVPattern.matcher(tvTest);

        count = 0;
        while(tNotVMatcher.find()){
            count++;
            System.out.println("Occurrence " + count + " : " + tNotVMatcher.start() + " to " + tNotVMatcher.end());
        }

        //American phone number (800) 123-4567
        String phone1 = "1234567890"; //Shouldn't match (no brackets and no dash
        String phone2 = "(123) 456-7890"; //Match
        String phone3 = "123 456-7890"; //Shouldn't match no brackets
        String phone4 = "(123)456-7890"; //Shouldn't match as there is no space
        //Homework: write a regular expression to validate these numbers, test against the four numbers

        System.out.println("phone1 " +
                phone1.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));

        System.out.println("phone2 " +
                phone2.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));

        System.out.println("phone3 " +
                phone3.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));

        System.out.println("phone4 " +
                phone4.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));

        //Visa number validation

        String visa1 = "4444444444444"; //should work
        String visa2 = "5444444444444"; //Shouldn't work
        String visa3 = "4444444444444444"; //Should work
        String visa4 = "4444"; //Should not match
        String visa5 = "4444444444444444444"; // Should work

        System.out.println("visa1 " + visa1.matches("^(4{1}[0-9]{12}$ | 4{1}[0-9]{15}$ | 4{1}[0-9]{18})"));
        System.out.println("visa2 " + visa2.matches("^(4{1}[0-9]{12}|4{1}[0-9]{15}|4{1}[0-9]{18}$)"));
        System.out.println("visa3 " + visa3.matches("^(4{1}[0-9]{12}|4{1}[0-9]{15}|4{1}[0-9]{18}$)"));
        System.out.println("visa4 " + visa4.matches("^(4{1}[0-9]{12}|4{1}[0-9]{15}|4{1}[0-9]{18}$)"));
        System.out.println("visa5 " + visa5.matches("^(4{1}[0-9]{12}|4{1}[0-9]{15}|4{1}[0-9]{18}$)"));

        System.out.println("visa3 " + visa3.matches("^4[0-9]{12}([0-9]{3})?([0-9]{3})?"));


        //Start of regex challenges
        //Challenge 1: "I want a bike."
        String challenge1 = "I want a bike.";

        System.out.println(challenge1.matches("I want a bike."));

        //Challenge 2
        String challenge2 = "I want a ball.";
        String regExpNick = "I want a .+\\.$";
        String regExpCameron = "[I want a].*?";
        String regExpKristine = "^I want a\\s{1}\\w+.$";
        String regExpOther = "I want a (bike|ball).";

        System.out.println(challenge1.matches(regExpKristine));
        System.out.println(challenge2.matches(regExpKristine));

        //Challenge 3 Do as challenge2 but use Pattern and Matcher classes
        regexChecker(regExpOther, challenge1);
        regexChecker(regExpOther, challenge2);

        //Challenge 4
        String challenge4 = "Replace all blanks with underscores.";
        System.out.println(challenge4.replaceAll(" ", "_"));

        //Challenge 5 - only the letters a-g are allowed and
        //there has to be at least one letter the regex
        //just has to match (not necessarily exactly)
        String challenge5 = "aaabcccccccccddefffg";

        //Challenge 6 - match challenge 5 exactly
        //has to be an exact match

        //Challenge 7 - Match a string starting with a series of
        //letters followed by a period, after the period there
        //must be a series of digits
        //kjisl.22 would match but f5.12a would not
        String challenge7 = "abcd.135";

        //Challenge 8 - Modify 7 to use a group, so that we
        //print all digits that occur in a string that contain
        //multiple occurrences of the pattern
        String challenge8 = "abcd.135uvqz.7tzik.999";
        //You should print out 135, 7, 999

        /* Challenge 9: Suppose you are reading strings that match patterns
        used in challenges 7 and 8 from a file. Tabs are used
        to separate the matches with one exception. The last
        match is followed by a newline
         */
        String challenge9 = "abcd.135\tuvqz.7\ttzik.999\n";
        //Revise the regular expression and extract all the numbers

        //Round 2
        /*Challenge 10 Instead of printing out the numbers
        print out their start and end indices. Use the same string as in 9. Make the indices inclusive e.g. start index printed for 135 would be 5 and the end should be 7
        Hint: Look at the documentation for Matcher.start() and Matcher.end()
         */

        /* Challenge 11 Suppose we have the following string containing points on a graph within curly braces. Extract what is in the curly braces*/
        String challenge11 = "{0, 2}, {0, 5}, {1, 3}, {2, 4}";

        /* Challenge 12 Write a regex to match a 5 digit US zip code
         */
        String challenge12 = "11111";

        /*Challenge 13 US zip codes can ve followed by a dash and another four numbers. Write regex for those zip codes*/
        String challenge13 = "11111-1111";

        /*Challenge14 Write a regex that matches both challenge 12 and 13*/



    }
    public static void regexChecker(String regExp, String toCheck){
        Pattern checkerPattern = Pattern.compile(regExp);
        Matcher regexMatcher = checkerPattern.matcher(toCheck);
        System.out.println(regexMatcher.matches());
    }
}
